import 'package:bmi/main.dart';
import 'package:bmi/pages/my_home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class BmiRef extends StatelessWidget {
  const BmiRef({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text('BMI Range Chart'),
        centerTitle: true,
        backgroundColor: Color(0xff090935),
      ),

      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(

                child: Image.asset('assets/img/bmi_range.png'),
              ),
            ),
            SizedBox(height: 50,),
            Padding(
              padding: EdgeInsets.all(10),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute( builder:(context)=>MyHomePage()));

                },
                child: Container(
                  width: 220,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xff090935),
                  ),

                  child: ListTile(
                    textColor: Colors.white,
                    title: Center(child: Text('Re-Calculate Bmi',
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 18,
                        fontWeight: FontWeight.bold,

                      ),)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),


    );
  }
}
