import 'package:bmi/bmi_ref.dart';
import 'package:bmi/formula.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'enum_file.dart';

class BmiResult extends StatefulWidget {


    double weight;
    double height;
    double inch;
     String bmiRange='';

    BmiResult(@required this.weight,@required this.height,@required this.inch);


  @override
  State<BmiResult> createState() => _BmiResultState();


}

class _BmiResultState extends State<BmiResult> {

  Logic logic=Logic();
  double result=0;
   @override
  void initState() {


     result=logic.bmiCalculate(widget.weight, widget.height, widget.inch);
     _bmiRange(result);
    


  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor:Color(0xff090935),
        appBar: AppBar(
          backgroundColor: Color(0xff090935),

          elevation: 0.0,
          title: Text('Bmi Result'),
          centerTitle: true,


        ),
        body: Center(
          child: SafeArea(
            child: Container(
              margin: EdgeInsets.all(20),
              width:400,
              height: 500,
               decoration: BoxDecoration(
                 color: Color(0xff050C51),
                 borderRadius: BorderRadius.circular(30)
               ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Your BMI Result',style: TextStyle(fontWeight:
                  FontWeight.bold,fontSize: 30,color: Colors.white),),
                  SizedBox(height: 20,),
                  Text('${result.toStringAsFixed(2)}',style: TextStyle(fontSize: 40,fontWeight:
                  FontWeight.bold,color: Colors.white),),
                    Text('"You are ${widget.bmiRange}"',style: TextStyle(
                       fontSize: 30,
                       fontWeight:FontWeight.bold,
                       color:Colors.white
                     ),

                    ),
                  SizedBox(height: 80,),

                  Padding(
                    padding: EdgeInsets.all(10),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute( builder:(context)=>BmiRef()));

                      },
                      child: Container(
                        width: 220,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Color(0xffDA0A57),
                        ),

                        child: ListTile(
                          textColor: Colors.white,
                          title: Center(child: Text('Bmi Range Refference',
                            maxLines: 1,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold

                          ),)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _bmiRange(double result) {

     if(result <=18.5){
       widget.bmiRange='Underweight';


     }
     else if(result>=18.5 && result <=24.9){
       widget.bmiRange='Healthy';

     }
     else if(result>=25 && result <=29.9){
       widget.bmiRange='Overweight';

     }
     else if(result>=30){
       widget.bmiRange='Obese';

     }


  }
}