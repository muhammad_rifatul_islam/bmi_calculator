import 'dart:math';

class Logic{

  double bmiCalculate(weight,height,inch){
    double cal_height=(height*12+inch)*0.0254;
    double bmi= weight/pow(cal_height, 2);
    return bmi;
  }
}