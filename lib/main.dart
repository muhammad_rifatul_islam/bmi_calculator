
import 'dart:ui';

import 'package:bmi/bmi_result.dart';
import 'package:bmi/pages/my_home_page.dart';
import 'package:bmi/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'enum_file.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);



  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false
      ,
      title: 'BMI',

      home:  SplashScreen()
    );
  }
}


