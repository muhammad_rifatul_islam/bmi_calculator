import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

import '../bmi_result.dart';
import '../enum_file.dart';

class MyHomePage extends StatefulWidget {


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Gender? selectedGender;
  int sliderValue=50;
  int height=4;
  int inch=6;

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      backgroundColor: Color(0xff090935),
      appBar: AppBar(title: const Text('BMI Calculator',style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 25
      ),),
        leading: Image.asset('assets/img/img.png',color: Colors.white,
        ),
        backgroundColor: Color(0xff090935),
        elevation:0.0,
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app), onPressed: () {

            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                return BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 6,sigmaY: 6),
                  child: AlertDialog(
                    title:   Text("Are you Sure To Exit !",style:GoogleFonts.lobster(
                      textStyle: TextStyle(
                        fontSize: 30
                      )
                    ),
                    textAlign: TextAlign.center,),

                    actions: <Widget>[
                      Lottie.asset('assets/animation/logout.json'),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                              style: TextButton.styleFrom(primary: Colors.blue),
                              onPressed: () {
                                SystemNavigator.pop();
                              },
                              child: const Text("Yes")),
                          TextButton(
                              style: TextButton.styleFrom(primary: Colors.blue),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text("No")),
                        ],
                      )


                    ],
                  ),
                );
              },


            );


          },
          )
        ],),
      body: SafeArea(

        child: Column(
          children: [
            Expanded(
              //first Row
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGender = Gender.Male;
                        });
                      },
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center
                          ,
                          children: [
                            Icon(FontAwesomeIcons.mars,
                              color: Colors.white,
                              size: 60,),
                            SizedBox(height: 10,),
                            Text('Male',style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 25
                            ),)
                          ],
                        ),
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border:Border.all(color: Color(0xff050C51),width:4),
                          borderRadius: BorderRadius.circular(16),
                          color:selectedGender==Gender.Male? Color(0xffDA0A57):Color(0xff090935),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGender = Gender.Female;
                        });
                      },
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center
                          ,
                          children: [
                            Icon(FontAwesomeIcons.venus,
                              color: Colors.white,
                              size: 60,),
                            SizedBox(height: 10,),
                            Text('Female',style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 25
                            ),)
                          ],
                        ),
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            border:Border.all(color: Color(0xff050C51),width:4) ,
                            borderRadius: BorderRadius.circular(16),
                            color:selectedGender==Gender.Female? Color(0xffDA0A57) : Color(0xff090935)
                        ),
                      ),
                    ),
                  ),


                ],
              ),
            ),

            //------------Second Row--------------------
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Weight',style: TextStyle(
                            color: Colors.white,

                            fontWeight: FontWeight.bold,
                            fontSize: 25,)
                          ),
                          Row(

                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            textBaseline: TextBaseline.alphabetic,
                            children: [
                              Text('$sliderValue',
                                style: TextStyle(fontSize: 55,fontWeight: FontWeight.bold,color: Colors.white),),
                              Text('kg' ,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold))
                            ],
                          ),
                          Slider(

                              value: sliderValue.toDouble(),
                              min: 10.0,
                              max: 200.0,
                              activeColor: Color(0xff050C51),
                              inactiveColor: Colors.white
                              ,

                              onChanged: (value){
                                setState((){

                                  sliderValue=value.toInt();
                                });

                              }
                          )

                        ],
                      ),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffDA0A57)


                      ),
                    ),
                  ),

                ],
              ),
            ),
            //--------third row---------
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffDA0A57)
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Height(Feet)',style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,)
                          ),
                          //----------3rd row inner row---------
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:[
                                Text('$height',style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 50,fontWeight: FontWeight.bold)
                                ),
                                Text(" ' ",style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold

                                ),)
                              ]),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  setState((){

                                    height++;
                                  });
                                },
                                child: CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child: Icon(FontAwesomeIcons.plus,color: Color(0xffDA0A57),),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  setState((){
                                    height--;
                                  });
                                },
                                child: CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child: Icon(FontAwesomeIcons.minus,color: Color(0xffDA0A57)),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  //<-------Third row second column----->
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Height(Inch.)',style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,)
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children:[
                                Text('$inch',style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 50,fontWeight: FontWeight.bold)
                                ),
                                Text(" '' ",style: TextStyle(
                                    fontSize: 30,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold

                                ),)
                              ]),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  setState((){
                                    inch++;
                                  });
                                },
                                child: CircleAvatar(

                                  backgroundColor: Colors.white,
                                  child: Icon(FontAwesomeIcons.plus,color: Color(0xffDA0A57)),
                                ),
                              ),
                              GestureDetector(
                                onTap: (){
                                  setState((){
                                    inch--;
                                  });
                                },
                                child: CircleAvatar(
                                  backgroundColor: Colors.white,
                                  child: Icon(FontAwesomeIcons.minus,color: Color(0xffDA0A57)),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color(0xffDA0A57)
                      ),
                    ),
                  ),


                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute( builder:(context)=>BmiResult(sliderValue.toDouble(),height.toDouble(),inch.toDouble())));

                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xffDA0A57),
                  ),

                  child: ListTile(
                    textColor: Colors.white,
                    title: Center(child: Text('Calculate Your Bmi',style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold
                    ),)),
                  ),
                ),
              ),
            ),




          ],

        ),
      ),

    );
  }
}