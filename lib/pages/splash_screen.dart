import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

import 'my_home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);



  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {



    Future.delayed(Duration(seconds: 6),
            (){
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context)=>MyHomePage()));
        }
    );

    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Color(0xff090935),

          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  Text("BMI",style:GoogleFonts.lobster(
                    textStyle: TextStyle(
                      fontSize: 80,
                      color: Colors.white
                    )
                  )
                  ),
                SizedBox(height: 40,),
                 Lottie.asset('assets/animation/loading.json',
                 height: 250)
              ],
            ),
          ),
        )

    );
  }
}
